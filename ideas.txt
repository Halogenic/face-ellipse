Represent genome/solution using a list of ellipses.

Each ellipse stores its data: size, colour, opacity.

Because of the nature of arrays/lists the order of each ellipse will matter
as their opacity will affect the final colour of pixels in the output image.

For crossover this means that swapping slices of each genome's list
of ellipses will result in more variation. In addition this means mutation
can include a component in which ellipses are swapped around.

Darker ellipses have more effect on the overall solution than lighter
ellipses especially if they are located earlier in the genome. This is because
compositing a light ellipse on top of a dark one does not lighten the pixels
any more. Pixels can only be darkened, not lightened.

Colour components (r, g, b, a) should be stored as a value between [-255, 255].
This will allow mutation to simply add or subtract a value from the colour
components and automatically "over-" or "under-flow" the components so that
they smoothly "ping-pong" between colours instead of drastically changing from
white to black (or inverse) when wrapping the value around.

Crossover: single-point crossover of each genome's array/list of ellipses. 
Single-point crossover will allow keeping the fit parts of a Solution's genome
as the order in which they appear is significant, so we don't want to screw
this order up too much.

Mutation: mutation should not always occur in new (or recombinated) Solutions
as this might reduce the breadth of the search space by disregarding the fitness
of best Solutions. This could be achieved by using a percentage mutation chance
or factor. Mutation should work on Ellipse values (position, dimensions and colour) and
it should also swap/delete/create new Ellipses in the genome. The percentage chance can
be applied to swapping/deletion/creation by choosing a number of ellipses equal to
(chance * len(ellipses)) and then deciding to swap, or delete and create new ones.

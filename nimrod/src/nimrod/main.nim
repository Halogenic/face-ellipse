proc stbi_load*(filename: ptr cstring, x: ptr cint, y: ptr cint, comp: ptr cint, req_comp: cint): ptr cstring
  {.cdecl, importc: "stbi_load", dynlib: "libstb_image.so".}


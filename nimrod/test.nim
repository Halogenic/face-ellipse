type
  TLibHandle = pointer

var
  RTLD_NOW {.importc: "RTLD_NOW", header: "<dlfcn.h>".}: int

proc dlclose(lib: TLibHandle) {.importc, header: "<dlfcn.h>".}
proc dlopen(path: cstring, mode: int): TLibHandle {.
  importc, header: "<dlfcn.h>".}

proc dlerror(): cstring {.importc, header: "<dlfcn.h>".}

proc nimUnloadLibrary(lib: TLibHandle) =
  dlclose(lib)

proc nimLoadLibrary(path: string): TLibHandle =
  result = dlopen(path, RTLD_NOW)

echo nimLoadLibrary("libstb_image.so").repr

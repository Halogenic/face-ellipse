// gcc test.c -o test -Llib/stb_image -lstb_image -lm
// need to link in libm for stb_image to correctly link

#include "stb_image.h"

#include <stdio.h>
#include <math.h>

int main() {
    printf("Hey there");

    double v = pow(2, 8);
    printf("%f", v);

    int x, y, comp;

    stbi_load("filename.png", &x, &y, &comp, 3);
}

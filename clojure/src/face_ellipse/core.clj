(ns face-ellipse.core
  (:import (java.awt AlphaComposite Color))
  (:import (java.awt.geom Ellipse2D))
  (:import (java.awt.image BufferedImage))
  (:import (java.io ByteArrayOutputStream File))
  (:import (javax.imageio ImageIO))
  (:import (java.lang Math)))

(defn abs
  "Wrap Math/abs in a Clojure function"
  [n]
  (Math/abs n))

(defn sum 
  "Sum a collection of values"
  [coll]
  (reduce + coll))

(defn rand-range
  "Generate a random number in the given range"
  [n1 n2]
  (+ n1 (rand-int (- n2 n1))))

(defn rand-ints
  "Infinite seq of random integers"
  [n]
  (cons (rand-int n) (lazy-seq (rand-ints n))))

(defn rand-range-ints
  "Infinite seq of random integers in the specified range"
  [n1 n2]
  (def n (rand-range-ints n1 n2))
  (cons n (lazy-seq (rand-range-ints n1 n2))))

(defn rand-choice
  "Return n randomly chosen items from coll"
  [n coll]
  (if (== n 1)
    (rand-nth (rand-int (count coll)))
  ())

(defn image-clear 
  "Clear the image to the specified value"
  [image value]
  (doseq [x (range (.getWidth image))]
    (doseq [y (range (.getHeight image))]
      (.setPixel (.getRaster image) x y (into-array Integer/TYPE [value value value 255])))))

(defn get-image-bytes
  "Get the array of bytes representing the image"
  [image]
  (def baos (new ByteArrayOutputStream))
  (ImageIO/write image "png" baos)
  (.flush baos)
  (def bytearray (.toByteArray baos))
  (.close baos)
  bytearray)

(defn image-draw-ellipse
  "Draw an oval onto the specified image"
  [image x y width height colour alpha]
  (def g (.createGraphics image))
  (.setColor g (new Color colour colour colour))
  (.setComposite g
    (AlphaComposite/getInstance AlphaComposite/SRC_OVER (/ alpha 255.0)))
  (.fillOval g x y width height))

(defn get-image-manhattan-norm
  "Get the manhattan norm of this image with another"
  [image image-ref]

  (def ref-bytes (get-image-bytes image-ref))
  (def this-bytes (get-image-bytes image))

  ; calculate the manhattan distance from the supplied reference image
  ; NOTE: must wrap Math/abs static method in #() in Clojure as we
  ; can't use a Java method like a Clojure function in map
  (/ 
    (sum 
      (map abs 
        (map #(- (first %) (second %)) (map vector this-bytes ref-bytes)))) ; sum of diff between two image byte values
    (* (.getWidth image) (.getHeight image)))) ; normalize by dividing by width * height

(defn save-image
  "Save the image to the specified path"
  [image path]
  (ImageIO/write image "png" (new File path)))

(defn create-ellipse-rand
  "Create a random ellipse constrained by image width and height"
  [image-w image-h]
  {
    :x (rand-int image-w) ; x coord of centre of ellipse
    :y (rand-int image-h) ; y coord of centre of ellipse
    :width (rand-range 1 image-w) ; width of ellipse
    :height (rand-range 1 image-h) ; height of ellipse
    :colour (rand-int 255)
    :alpha (rand-int 255)
  })

;(defn ellipse-continuum
(defn infinite-ellipses
  "Generate a lazy seq of infinite random ellipses"
  [image-w image-h]
  (def ellipse (create-ellipse-rand image-w image-h))
  (cons ellipse (lazy-seq (infinite-ellipses image-w image-h))))

(defn create-solution-rand
  "Create a random solution with the desired image width and height"
  [image-w image-h ellipse-count]

  ; generate random ellipses
  (def ellipses
    (for [i (range ellipse-count)]
      (create-ellipse-rand image-w image-h)))
  
  { :image-width image-w :image-height image-h :ellipses ellipses })

(defn get-solution-image
  "Get the solution's image"
  [solution]
  (def image 
    (new BufferedImage (:image-width solution) (:image-height solution) BufferedImage/TYPE_BYTE_GRAY))
  (image-clear image 255)
  
  ; draw ellipses onto image
  (doseq [e (:ellipses solution)]
    (def x (- (:x e) (/ (:width e) 2)))
    (def y (- (:y e) (/ (:height e) 2)))
    (image-draw-ellipse image x y (:width e) (:height e) (:colour e) (:alpha e)))

  image)

(defn get-solution-fitness
  [solution image-ref]
  (def image (get-solution-image solution))
  (get-image-manhattan-norm image image-ref))

(defn save-solution
  "Save the solution to the specified path"
  [solution path]
  (def image (get-solution-image solution))
  (save-image image path))

(defn solution-mutate
  "Mutates a copy of the given solution"
  [solution factor]

  (def amount (int (* (count (:ellipses solution)) factor)))

  ; choose some ellipses and replace them with new random ones
  (def indexes (take amount (rand-ints )))
  
(defn -main
  [& args]
  (println (take 3 (rand-ints 10 20)))
  (println (take 2 (infinite-ellipses 50 50))))

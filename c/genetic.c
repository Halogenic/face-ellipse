#include <FreeImage.h>

#include <stdio.h>

typedef struct {
	int x;
	int y;
	int width;
	int height;
	int colour;
	int alpha;
} Ellipse;

int main() {
	FreeImage_Initialise(TRUE);

	FIBITMAP *ref_image = FreeImage_Load(FIF_PNG, "../richard-model-face.png", PNG_DEFAULT);
	FIBITMAP *ptr = FreeImage_ConvertTo8Bits(ref_image);

	FreeImage_Unload(ref_image);
	ref_image = ptr;

	FIBITMAP *out_image = FreeImage_Allocate(FreeImage_GetWidth(ref_image), FreeImage_GetHeight(ref_image), 8, FI_DEFAULT(0), FI_DEFAULT(0), FI_DEFAULT(0));
	FreeImage_Save(FIF_PNG, out_image, "test.png", FI_DEFAULT(0));
	FreeImage_Unload(out_image);


	FreeImage_DeInitialise();
}
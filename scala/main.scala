import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import scala.math
import scala.util.Random

import java.awt.{AlphaComposite, Color}
import java.awt.geom.Ellipse2D
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.File
import javax.imageio.ImageIO

import org.imgscalr.Scalr._
// import java.lang.Math


object Globals {
	val random = new Random()

  def sample[T](population: Seq[T], k: Int): Seq[T] = {
    "Samples k elements from Seq population without replacement"

    val buffer = population.toBuffer

    val sampled = ArrayBuffer[T]()

    for (i <- 1 to k) {
      val index = random.nextInt(buffer.length)
      sampled.append(buffer.remove(index))
    }

    sampled
  }

  def randInt(end: Int): Int = {
    random.nextInt(end)
  }

  def randrange(start: Int, stop: Int): Int = {
    (start + random.nextDouble() * (stop - start)).toInt
  }
}


class Colour(var ri: Int, var gi: Int, var bi: Int, var ai: Int) {
  def r: Int = ri.abs
  def g: Int = gi.abs
  def b: Int = bi.abs
  def a: Int = ai.abs

  def mutate(factor: Double = 0.02) = {
    ri = _wrap((ri + Globals.random.nextInt(10) - 5))
    gi = _wrap((gi + Globals.random.nextInt(10) - 5))
    bi = _wrap((bi + Globals.random.nextInt(10) - 5))
    ai = _wrap((ai + Globals.random.nextInt(10) - 5))
  }
  
  def _wrap(i: Int): Int = {
    if (i > 255) {
      -255 + (i.abs % 256)
    } else if (i < -255) {
      255 - (i.abs % 256)
    } else {
      i
    }
  }

  def color: Color = new Color(r, g, b, a)

  override def toString = {
    "(%d, %d, %d, %d)".format(r, g, b, a)
  }
}


class Image(val image: BufferedImage) {

	def width: Int = image.getWidth()
	def height: Int = image.getHeight()

	def getManhattanNormWith(other: Image): Double = {
		"Get the normalized manhattan distance with another image"
		val otherValues = for (b <- other.toBytes) yield b.toInt
		val myValues = for (b <- toBytes) yield b.toInt

		// calculate manhattan distance
		val dist = myValues
		  .zip(otherValues)
		  .map( { case (v1: Int, v2: Int) => math.abs(v1 - v2) } )
		  .sum

		// normalize by dividing by number of pixels
		dist.toDouble / (width * height)
	}

	def drawEllipse(e: Ellipse) = {
		"Draw an ellipse onto the image"
		val g = image.createGraphics()
    val ac = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, e.colour.a.toFloat / 255.0f)
		g.setColor(e.colour.color)
    g.setComposite(ac)
    val (x, y) = e.origin
		g.fillOval(x, y, e.width, e.height)
	}

	def toBytes: Array[Byte] = {
		"Convert the image into an array of bytes"
		val baos = new ByteArrayOutputStream()
		ImageIO.write(image, "png", baos)
		baos.flush()
		val bytes = baos.toByteArray()
		baos.close()

		bytes
	}

  def clear(value: Int) = {
    "Clear the image to the specified value"

    for (i <- 0 until image.getWidth(); j <- 0 until image.getHeight()) {
      image.getRaster().setSample(i, j, 0, value)
    }
  }
}


class Ellipse(centre: Tuple2[Int, Int], dimensions: Tuple2[Int, Int], var colour: Colour) {
	"Defines an ellipse given position, dimensions and colour"
	var (x, y) = centre
	var (width, height) = dimensions

  def origin: Tuple2[Int, Int] = (x - width / 2, y - height / 2)

  def mutate(factor: Double = 0.02) = {
    x += Globals.random.nextInt(10) - 5
    y += Globals.random.nextInt(10) - 5
    width += Globals.random.nextInt(10) - 5
    height += Globals.random.nextInt(10) - 5
    colour.mutate()
  }

  override def toString = {
    "{(%d, %d), (%d, %d), %s}".format(x, y, width, height, colour.toString)
  }
}


class Solution(imageSize: Tuple2[Int, Int]) {
	val (imageWidth, imageHeight) = imageSize

	val maxEllipseCount = 100
	var ellipses = Array[Ellipse]()

	def randomize() = {
		"Generate random ellipses"

		// generate random sequence of ellipses
		val ellipseSequence = for {
			i <- 1 to maxEllipseCount
		} yield getRandomEllipse

		ellipses = ellipseSequence.toArray
	}

	def getRandomEllipse: Ellipse = {
		val pos = getEllipsePosition
		val dim = getEllipseDimensions
		val c = Globals.random.nextInt(256)
		val a = Globals.random.nextInt(256)
		new Ellipse(pos, dim, new Colour(c, c, c, a))
	}

	def getEllipsePosition: Tuple2[Int, Int] = {
		"Generate random ellipse position"
		val x = Globals.random.nextInt(imageWidth)
		val y = Globals.random.nextInt(imageHeight)
		(x, y)
	}

	def getEllipseDimensions: Tuple2[Int, Int] = {
		"Generate random ellipse width and height"
    
    // NO WE DON'T!! 
    // we want to favour smaller ellipses in general
    // so use a gaussian distribution with mean 0.0
    // and standard deviation of 25 to keep ellipse
    // dimensions centred around 0
    
//    val width = (Globals.random.nextGaussian() * 25).toInt
//    val height = (Globals.random.nextGaussian() * 25).toInt
    
    // cannot have width/height of 0
		val width = Globals.randrange(1, imageWidth)
		val height = Globals.randrange(1, imageHeight)
		(width, height)
	}

	def image: Image = {
		"Generate the image solution from the existing ellipses"
		val image = new Image(new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_BYTE_GRAY))
    image.clear(255)

		for (e <- ellipses) {
			image.drawEllipse(e)
		}

		image
	}

	def mutate(chance: Double = 0.02) = {
    "Mutate this Solution"
    val count = (chance * ellipses.length).toInt

    // replace ellipses
    for (i <- 1 to count) {
      val index = Globals.random.nextInt(maxEllipseCount)
      val ellipse = getRandomEllipse
      ellipses(index) = ellipse
    }

    // swap ellipses
    for (i <- 1 to count) {
      val index1 = Globals.random.nextInt(maxEllipseCount)
      val index2 = Globals.random.nextInt(maxEllipseCount)
      val temp = ellipses(index1)
      ellipses(index1) = ellipses(index2)
      ellipses(index2) = temp
    }

    // change some ellipses randomly
    for (i <- 1 to count) {
      val index = Globals.randInt(ellipses.length)
      ellipses(index).mutate()
    }
	}

	def crossover(other: Solution): Tuple2[Solution, Solution] = {
		// perform single-point crossover

		val index = Globals.random.nextInt(ellipses.length)

		val (left1, right1) = ellipses.splitAt(index)
    val (left2, right2) = other.ellipses.splitAt(index)
        
    var child1 = new Solution((imageWidth, imageHeight))
    var child2 = new Solution((imageWidth, imageHeight))

    child1.ellipses = left1 ++ right2
    child2.ellipses = left2 ++ right1

    (child1, child2)
	}

  var _fitness = Double.MaxValue

	def fitness(refImage: Image) = {
		"Calculate the fitness of this solution (manhattan distance normalized)"

    if (_fitness == Double.MaxValue)	{ 
      // cache the fitness to speed up future lookups
      _fitness = image.getManhattanNormWith(refImage)
    }
    _fitness
	}

	def save(path: String) = {
		"Save the solution image to the given file path"
		val file = new File(path)
		ImageIO.write(image.image, "png", file)

    // also save a JSON file which stores the ellipses for this solution

	}

  def markAsDirty() = _fitness = Double.MaxValue
}


class Population(val refImage: Image, val count: Int = 10) {
  var solutions = Array[Solution]()

  def init() = {
    val solutionSeq = for { i <- 1 to count } yield getRandomSolution
    solutions = solutionSeq.toArray
  }

  def select(num: Int = 2): Seq[Solution] = {
    "Select a Seq of Solutions from the population using tournament selection"

    val buffer = solutions.toBuffer

    // Select 'num' solutions without replacement into the array
    val selected = ArrayBuffer[Solution]()

    for (i <- 1 to num) {
      val Seq(firstIndex, secondIndex) = Globals.sample(0 until buffer.length, 2)
      val first = buffer(firstIndex)
      val second = buffer(secondIndex)
      if (first.fitness(refImage) < second.fitness(refImage)) {
        selected.append(buffer.remove(firstIndex))
      } else {
        selected.append(buffer.remove(secondIndex))
      }
    }

    selected
  }

  def crossover(selected: Seq[Solution], mutateChance: Double = 0.02): Seq[Solution] = {
    """
    Returns a sequence of child solutions that were produced using
    the crossover function between the list of selected solutions.
    """
    
    // apply crossover to all pairs of selected parent solutions
    val childrenTuples = for {
      combi <- selected.combinations(2)
      val Seq(p1, p2) = combi
    } yield p1.crossover(p2)

    // now flatten the sequence of tuples
    // map to convert to Seq of Seqs
    // then reduce using the Seq concatenate operator
    childrenTuples.map( { case (c1: Solution, c2: Solution) => Seq(c1, c2) } )
                  .reduce(_ ++ _)
  }

  def replace(replacements: Seq[Solution]) = {
    val newSolutions = solutions.toBuffer

    // replace the solutions worse than the given replacements
    for (replacement <- replacements) {
      val worse = newSolutions.filter(s => replacement.fitness(refImage) < s.fitness(refImage))
                              .sortBy(s => s.fitness(refImage)) // this will sort lowest -> highest

      if (!worse.isEmpty) {
        val worst = worse.last

        // check if replacing this solution would result in 
        // overall more diversity otherwise do not replace
//        val worstDiversity = getDiversity(worst)

//        val popcopy = new Population(refImage, count)
//        popcopy.solutions = solutions.filter(s => s != worst)
//        val replacementDiversity = popcopy.getDiversity(replacement)
        
//        if (replacementDiversity > worstDiversity) {
        newSolutions.update(newSolutions.indexOf(worst), replacement)
//        }
      }
    }

    solutions = newSolutions.toArray
  }

  def evolve(selectionSize: Int = 2, mutateChance: Double = 0.02) = {
    val selected = select(selectionSize)
//    println("Selected: [%s]".format((for (sol <- selected) yield sol.fitness(refImage)).mkString(" ")))
    val children = crossover(selected, mutateChance)

    // mutate all children
    for (sol <- children) {
      sol.mutate(mutateChance)
    }

//    println("Children: [%s]".format((for (sol <- children) yield sol.fitness(refImage)).mkString(" ")))
    replace(children)
  }

  def getFittest: Solution = {
    solutions.reduce((s1, s2) => if (s1.fitness(refImage) < s2.fitness(refImage)) s1 else s2)
  }

  def getRandomSolution: Solution = {
    val solution = new Solution((refImage.width, refImage.height))
    solution.randomize()
    solution
  }

  def getDiversity(s: Solution): Double = {
    "Returns the difference between this solution 's' and the population"
    (for { sol <- solutions if s != sol } yield s.image.getManhattanNormWith(sol.image) ).sum
  }

  override def toString = {
    "[%s]".format((for (sol <- solutions) yield sol.fitness(refImage)).mkString(" "))
  }
}


object Main {

  def parseArgs(args: Array[String]): Map[Symbol, Any] = {
    val usage = "Usage: main <filename> [options]"

    if (args.length == 0) println(usage)
    
    val arglist = args.toList
    type OptionMap = Map[Symbol, Any]

    def nextOption(map: OptionMap, list: List[String]): OptionMap = {
      def isSwitch(s: String) = (s(0) == '-')
      list match {
        case Nil => map
        case "-n" :: value :: tail => nextOption(map ++ Map('iterations -> value.toInt), tail)
        case "-p" :: value :: tail => nextOption(map ++ Map('populationCount -> value.toInt), tail)
        case "-m" :: value :: tail => nextOption(map ++ Map('mutation -> value.toDouble), tail) // mutation rate
        case "--stop" :: value :: tail => nextOption(map ++ Map('stop -> value.toDouble), tail) // the stop criterion (fitness value)
        case string :: opt2 :: tail if isSwitch(opt2) => nextOption(map ++ Map('infile -> string), list.tail)
        case string :: Nil => nextOption(map ++ Map('infile -> string), list.tail)
        case option :: tail => println("Unknown option " + option); exit(1)
      }
    }

    nextOption(Map(), arglist)
  }

	def main(args: Array[String]) {
    val options = parseArgs(args)

		val refImage = new Image(ImageIO.read(new File(options('infile).toString)))

    val iterations = if (options.contains('iterations)) options('iterations).asInstanceOf[Int] else 10

    val populationCount = if (options.contains('populationCount)) options('populationCount).asInstanceOf[Int] else 10

    val mutationRate = if (options.contains('mutation)) options('mutation).asInstanceOf[Double] else 0.02

    val stopFitness = if (options.contains('stop)) options('stop).asInstanceOf[Double] else 4.0

    var fittest: Solution = null
    var bestFitness = Double.MaxValue
    var eIndex = 1

    while (bestFitness > stopFitness) {
      val population = new Population(refImage, populationCount)
      population.init()

      println("Epoch: %d".format(eIndex))

      for (i <- 1 to iterations) {
        println("Iteration: %d".format(i))
        population.evolve(mutateChance = mutationRate)
        // println(population.toString)
        println("Fitness: %s".format(population.getFittest.fitness(refImage).toString))
      }

      val popFittest = population.getFittest

      fittest = fittest match {
        case null => popFittest
        case _    => if (popFittest.fitness(refImage) < bestFitness) popFittest else fittest
      }

      if (popFittest == fittest) {
        // only write to file if the fittest has changed
        bestFitness = fittest.fitness(refImage)
        fittest.save("best.png")
      }
      println("Best so far: %s".format(bestFitness.toString))

      eIndex += 1
    }

    println("Success! Found a solution with fitness %s in %d epochs!".format(bestFitness.toString, eIndex))
	}
}

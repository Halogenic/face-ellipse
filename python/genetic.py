
import argparse
import copy
import json
import random
import sys

from itertools import combinations
from operator import methodcaller

import PIL.Image

from image import Image
from solution import Solution
from ellipse import Ellipse


class SolutionGA(Solution):

    def __init__(self, image_size):
        super(SolutionGA, self).__init__(image_size)

        self._fitness = float("inf")

    def mutate(self, rate=0.02):
        count = int(rate * len(self.ellipses))

        # replace ellipses
        for index in random.sample(xrange(len(self.ellipses)), count):
            self.ellipses[index] = Ellipse.create_random(self.image_size)

        # swap ellipses
        # for _ in xrange(count):
        #     index1, index2 = random.sample(xrange(len(self.ellipses)), 2)
        #     temp = self.ellipses[index1]
        #     self.ellipses[index1] = self.ellipses[index2]
        #     self.ellipses[index2] = temp

        # # change some ellipses
        # for _ in xrange(count):
        #     index = random.randint(0, len(self.ellipses) - 1)

        #     ellipse = copy.copy(self.ellipses[index])
        #     ellipse.x += random.randrange(-5, 6)
        #     ellipse.y += random.randrange(-5, 6)
        #     ellipse.width += random.randrange(-5, 6)
        #     ellipse.height += random.randrange(-5, 6)
        #     ellipse.colour = self._wrap_colour_component(ellipse.colour + random.randrange(-5, 6))
        #     ellipse.alpha = self._wrap_colour_component(ellipse.alpha + random.randrange(-5, 6))

        #     self.ellipses[index] = ellipse

    def _wrap_colour_component(self, value):
        # keep colour values ping ponging
        if value > 255:
            return abs(-255 + (abs(value) % 256))
        elif value < -255:
            return 255 - (abs(value) % 256)
        
        return value

    def crossover(self, other):
        point = random.randint(0, len(self.ellipses) - 1)

        left1 = self.ellipses[:point]
        right1 = self.ellipses[point:]
        left2 = other.ellipses[:point]
        right2 = other.ellipses[point:]

        child1 = SolutionGA(self.image_size)
        child2 = SolutionGA(self.image_size)

        child1.ellipses = left1 + right2
        child2.ellipses = right2 + left2

        return (child1, child2)

    def fitness(self, ref_image):
        if (self._fitness == float("inf")):
            self._fitness = self.distance_from(ref_image)
        return self._fitness

    @staticmethod
    def create_random(image_size):
        solution = SolutionGA(image_size)
        solution.randomize()
        return solution


class Population(object):

    def __init__(self, ref_image, count=10):
        self.ref_image = ref_image
        self.count = count

        self.solutions = [SolutionGA.create_random(ref_image.size) for i in xrange(count)]

    def select(self, count=2):
        selected = []

        for i in xrange(count):
            # this allows one solution to be both parents of new child solution
            candidates = (random.choice(self.solutions), random.choice(self.solutions))

            if candidates[0].fitness(self.ref_image) < candidates[1].fitness(self.ref_image):
                selected.append(candidates[0])
            else:
                selected.append(candidates[1])

        return selected

    def crossover(self, selected, mutation_rate=0.02):
        children = []

        for combo in combinations(selected, 2):
            children.append(combo[0].crossover(combo[1]))

        # flatten list of children tuples
        return reduce(lambda x,y: list(x) + list(y), children)

    def replace(self, replacements):
        for replacement in replacements:
            worse = [sol for sol in self.solutions if sol.fitness(self.ref_image) > replacement.fitness(self.ref_image)]
            if worse:
                worst = sorted(worse, key=methodcaller("fitness", self.ref_image))[-1]
                self.solutions[self.solutions.index(worst)] = replacement

    def evolve(self, selection_size=2, mutation_rate=0.02):
        selected = self.select(selection_size)
        children = self.crossover(selected, mutation_rate)

        for child in children:
            child.mutate(mutation_rate)

        self.replace(children)

    def fittest(self):
        return iter(self).next()

    def __getitem__(self, index):
        pass

    def __iter__(self):
        return iter(sorted(self.solutions, key=methodcaller("fitness", self.ref_image)))

    def __len__(self):
        return len(self.solutions)

    def __str__(self):
        return "[%s]" % " ".join([str(sol.fitness(self.ref_image)) for sol in self])

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("settings", nargs=2)
    parser.add_argument("-p", "--pop-count", action="store", type=int, default=10)
    parser.add_argument("-m", "--mutation-rate", action="store", type=float, default=0.02)
    parser.add_argument("--start", action="store", help="A json file representing a solution to start the search from")
    parser.add_argument("--stop", action="store", type=float, default=40)
    parser.add_argument("-o", "--output", action="store", default="best.png")

    return parser.parse_args()

def main():
    args = parse_args()

    filename, iterations = args.settings[0], int(args.settings[1])

    image = PIL.Image.open(filename).convert("L")
    ref_image = Image(image.size, image)

    fittest = None
    best_fitness = float("inf")

    epoch = 1
    while best_fitness > args.stop:
        population = Population(ref_image, args.pop_count)

        print "Epoch: %d" % epoch

        for i in xrange(iterations):
            print "Iteration: %d" % i
            population.evolve(2, args.mutation_rate)
            # print population
            # print "Average fitness: %s" % str(reduce(lambda x,s: x + s.fitness(ref_image), population.solutions, 0.0) / len(population))
            print "Fitness: %s" % str(population.fittest().fitness(ref_image))

        pop_fittest = population.fittest()

        if not fittest:
            fittest = pop_fittest
        elif pop_fittest.fitness(ref_image) < best_fitness:
            fittest = pop_fittest

        if fittest == pop_fittest:
            best_fitness = fittest.fitness(ref_image)
            fittest.save(args.output)

        epoch += 1

    print "Success! Found a solution with fitness %s in %d epochs" % (str(best_fitness), epoch)

    fittest.save(args.output)
    fittest.show()

if __name__ == "__main__":
    main()

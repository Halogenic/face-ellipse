
import json
import os

from common import MAX_ELLIPSE_COUNT
from ellipse import Ellipse
from image import Image


class Solution(object):
	
	def __init__(self, image_size):
		self.ellipses = []

		self.image_size = image_size

		self._image = None

	def image(self):
		if not self._image:
			self._image = Image(self.image_size)

			for ellipse in self.ellipses:
				self._image.draw_ellipse(ellipse)

		return self._image

	def randomize(self):
		for i in xrange(MAX_ELLIPSE_COUNT):
			ellipse = Ellipse.create_random(self.image_size)
			self.ellipses.append(ellipse)

	def distance_from(self, ref_image):
		return self.image().get_manhattan_norm_with(ref_image)

	def save(self, path):
		"Save the image to file at 'path'"
		self.image().save(path)

		# also save out json file with ellipses in it
		with open(os.path.join(os.path.splitext(path)[0] + ".json"), "w") as json_file:
			json_list = [(e.x, e.y, e.width, e.height, e.colour, e.alpha) for e in self.ellipses]
			json.dump(json_list, json_file)

	def show(self):
		self.image().show()

	@staticmethod
	def create_random(image_size):
		solution = Solution(image_size)
		solution.randomize()
		return solution

	@staticmethod
	def from_json(json_list):
		solution = Solution(self.image_size)
		for x, y, width, height, colour, alpha in json_list:
			solution.ellipses.append(Ellipse(x, y, width, height, colour, alpha))

		return solution

import PIL.Image

from PIL import ImageDraw

from scipy import sum
from scipy.linalg import norm
from pylab import array


class Image(object):

	def __init__(self, size, image=None):
		self._image = image or PIL.Image.new("L", size, 255)

	@property
	def size(self):
		return self._image.size

	def draw_ellipse(self, ellipse):
		"""
		Draw an ellipse into the image.
		"""

		bbox = (ellipse.origin[0], ellipse.origin[1], ellipse.width, ellipse.height)

		# create an image holding the ellipse's colour to be applied with the mask
		colour_image = PIL.Image.new("L", self.size, ellipse.colour)
		mask = PIL.Image.new("L", self.size, 0)
		draw = ImageDraw.Draw(mask)
		# draw the alpha value into the mask
		draw.ellipse(bbox, fill=ellipse.alpha)

		self._image.paste(colour_image, (0, 0), mask)

	def get_manhattan_norm_with(self, other):
		flat = array(self._image).flatten()
		other_flat = array(other._image).flatten()

		dist = sum(abs(flat - other_flat))

		return dist / flat.size

	def save(self, path):
		self._image.save(path)

	def show(self):
		self._image.show()
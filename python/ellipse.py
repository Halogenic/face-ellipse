
import random


class Ellipse(object):

	def __init__(self, x, y, width, height, colour, alpha):
		self.x = x
		self.y = y
		self.width = width
		self.height = height
		self.colour = colour
		self.alpha = alpha

	@property
	def centre(self):
		return (self.x, self.y)

	@property
	def origin(self):
		return (self.x - self.width / 2, self.y - self.height / 2)

	@property
	def dimensions(self):
		return (self.width, self.height)

	def __copy__(self):
		return Ellipse(self.x, self.y, self.width, self.height, self.colour, self.alpha)

	@staticmethod
	def create_random(image_size):
		image_width, image_height = image_size

		x = random.randint(0, image_width)
		y = random.randint(0, image_height)
		width = random.randint(1, image_width)
		height = random.randint(1, image_height)
		colour = random.randint(0, 255)
		alpha = random.randint(0, 255)

		return Ellipse(x, y, width, height, colour, alpha)

import argparse
import copy
import math
import random
import sys

import PIL.Image

from ellipse import Ellipse
from image import Image
from solution import Solution


class SolutionAnneal(Solution):

	def create_neighbour(self, rate=0.1):
		neighbour = SolutionAnneal(self.image_size)

		ellipses = copy.copy(self.ellipses)

		# create new ellipses and replace existing ones
		count = int(rate * len(ellipses))
		for index in random.sample(xrange(len(ellipses)), count):
			ellipses[index] = Ellipse.create_random(self.image_size)

		neighbour.ellipses = ellipses

		return neighbour

	def energy(self, ref_image):
		return self.distance_from(ref_image)

	@staticmethod
	def create_random(image_size):
		solution = SolutionAnneal(image_size)
		solution.randomize()
		return solution


def acceptance_p(energy, new_energy, temp):
	# accept new solution if it is better
	if new_energy < energy:
		return 1.0

	return math.exp((energy - new_energy) / temp)

def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument("settings", nargs=1)
    parser.add_argument("-t", "--temperature", action="store", type=float, default=10000)
    parser.add_argument("-r", "--cooling-rate", action="store", type=float, default=0.003)

    return parser.parse_args()

def main():
	args = parse_args()

	filename = args.settings[0]
	temp = args.temperature

	image = PIL.Image.open(filename).convert("L")
	ref_image = Image(image.size, image)

	current = SolutionAnneal.create_random(ref_image.size)

	best = current

	print "Starting energy: %s" % str(best.energy(ref_image))

	while temp > 1:
		# print current.energy(ref_image), best.energy(ref_image)
		print "Temp: %s" % str(temp)
		neighbour = current.create_neighbour()

		if acceptance_p(current.energy(ref_image), neighbour.energy(ref_image), temp) >= random.random():
			current = neighbour

		if current.energy(ref_image) < best.energy(ref_image):
			best = current

		temp *= 1 - args.cooling_rate

	print "Final solution energy: %s" % str(best.energy(ref_image))
	best.save("final.png")


if __name__ == "__main__":
	main()